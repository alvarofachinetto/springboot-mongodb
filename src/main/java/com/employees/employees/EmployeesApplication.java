package com.employees.employees;

import com.employees.employees.employee.Employee;
import com.employees.employees.employee.EmployeeRepository;
import lombok.AllArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

@SpringBootApplication
@AllArgsConstructor
public class EmployeesApplication implements CommandLineRunner {

	private final EmployeeRepository repository;

	public static void main(String[] args) {
		SpringApplication.run(EmployeesApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		List<Employee> employees = List.of(new Employee("Alvaro", LocalDate.of(2000,1,24), new BigDecimal("15000")),
				new Employee("Gustavo", LocalDate.of(2000,3,15), new BigDecimal("2000")));

		repository.saveAll(employees);

	}
}
