package com.employees.employees.employee;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.math.BigDecimal;
import java.time.LocalDate;

@Document
@Data
public class Employee {

    @Id
    private String id;

    private String name;

    private LocalDate born;

    private BigDecimal salary;

    public Employee() {}

    public Employee(String name, LocalDate born, BigDecimal salary) {
        this.name = name;
        this.born = born;
        this.salary = salary;
    }
}
